from django.test import TestCase

from ...models import Hero


class HeroTests(TestCase):

    def test_was_created(self):
        h = Hero.objects.create(id=1, name='name', gender='F', movie='My movie')
        self.assertEquals(Hero.objects.get(pk=1), h)
