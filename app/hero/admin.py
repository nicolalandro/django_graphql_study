from django.contrib import admin
from .models import Hero


class HeroAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'movie']
    search_fields = ['name', 'movie']
    list_filter = ['gender']


admin.site.register(Hero, HeroAdmin)
