import graphene
from hero import graphene_schema as hero_schema


class Query(hero_schema.Query, graphene.ObjectType):
    pass


schema = graphene.Schema(query=Query)
