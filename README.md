[![pipeline status](https://gitlab.com/nicolalandro/django_graphql_study/badges/master/pipeline.svg)](https://gitlab.com/nicolalandro/django_graphql_study/commits/master)
[![coverage report](https://gitlab.com/nicolalandro/django_graphql_study/badges/master/coverage.svg)](https://gitlab.com/nicolalandro/django_graphql_study/commits/master)


# Studio Django e cose utili

## Creare app django
fonti:
* (1)

```
pip install django
django-admin startproject app
python manage.py createsuperuser
```

## Aggiungere GraphQl
fonti:
* (2)
* (8)
* (9)

seguire tutorial (8)
```
cd app
python manage.py runserver
```

## Sistemare admin per vedere e cercare i risultati
fonti:
* (7)

## Aggiungere index a progetto hero
fonti:
* (3)
* (4)

## Index estende base html che importa vue, buefy e fontawesome
fonti:
* (10)
* (14)
* (15)

## Use vue.js & Buefy
* (10)
* (11)
* (13)
* (14)

## Api call with axios and use bulma and vue
* (10)
* (12)
* (13)
* (14)

## Test python
fonti:
* (16)
* (17)
* (5)

gitlab integration:
```
./push_docker_test_container.sh
# guarda il .gitlab-ci.yml, e il TestDockerfile
# usa i badge per vedere i risultati
```

# Bibliografia:
* (1) [Writing your first Django app, part 1](https://docs.djangoproject.com/en/2.2/intro/tutorial01/): Get started
* (2) [Writing your first Django app, part 2](https://docs.djangoproject.com/en/2.2/intro/tutorial02/): DB e Admin
* (3) [Writing your first Django app, part 3](https://docs.djangoproject.com/en/2.2/intro/tutorial03/): Rotte & HTML
* (4) [Writing your first Django app, part 4](https://docs.djangoproject.com/en/2.2/intro/tutorial04/): Views e HTML approfondito
* (5) [Writing your first Django app, part 5](https://docs.djangoproject.com/en/2.2/intro/tutorial05/): Test
* (6) [Writing your first Django app, part 6](https://docs.djangoproject.com/en/2.2/intro/tutorial06/): Look and Feel
* (7) [Writing your first Django app, part 7](https://docs.djangoproject.com/en/2.2/intro/tutorial07/): Admin custom

* (8) [GraphQL - A query language for your API](https://graphql.org/)
* (9) [GraphQL with Django — a tutorial that works](https://medium.com/@alhajee2009/graphql-with-django-a-tutorial-that-works-2812be163a26)

* (10) [Vue.js introduction](https://vuejs.org/v2/guide/)
* (11) [How to change delimiters in vue.js](https://stackoverflow.com/questions/48125577/how-to-change-delimiters-in-vue-js#48125608)

* (12) [Axios tutorial](http://zetcode.com/javascript/axios/)

* (13) [Bulma](https://bulma.io/)
* (14) [Buefy](https://buefy.org/)
* (15) [Font Awesome](https://fontawesome.com/icons?d=gallery)

* (16) [DevOps Playbook. Docker e i container applicativi.](https://medium.com/@fminzoni/devops-playbook-docker-e-i-container-applicativi-a0731ec1de3d)

* (17) [Test and deploy a Python application with GitLab CI/CD](https://docs.gitlab.com/ee/ci/examples/test-and-deploy-python-application-to-heroku.html)
