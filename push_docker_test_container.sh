PROJECT_USER="nicolalandro"
PROJECT_NAME="django_graphql_study"
REGISTRY_BASE_URL="registry.gitlab.com"
CONTAINER_NAME="test_python"

docker login $REGISTRY_BASE_URL
docker build -t $REGISTRY_BASE_URL/$PROJECT_USER/$PROJECT_NAME/$CONTAINER_NAME -f TestDockerfile .
docker push $REGISTRY_BASE_URL/$PROJECT_USER/$PROJECT_NAME/$CONTAINER_NAME